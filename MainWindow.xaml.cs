﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HeartMeterClient.SignalR.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        HubConnection hubConnection;
        public MainWindow()
        {
            InitializeComponent();
            hubConnection = new HubConnectionBuilder().WithUrl("http://192.168.1.105:5000/hrmHub").Build();
            hubConnection.On<int>("HrmFromServer", value =>
            {
                Dispatcher.Invoke(() =>
                {
                    BPM.Text = value.ToString();
                });                
            });
            Loaded += MainWindow_Loaded;
            KeyDown += MainWindow_KeyDown;
        }

        private async void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                await ConnectAsync();
            }
        }

        private async void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            await ConnectAsync();
        }

        async Task ConnectAsync()
        {
            try
            {
                await hubConnection.StartAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
